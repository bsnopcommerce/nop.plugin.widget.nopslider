﻿using Nop.Plugin.Widget.NopSlider.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Widget.NopSlider.Data
{
    public class ItemMap : EntityTypeConfiguration<Item>
    {
        public ItemMap()
        {
            ToTable("NopSlider_NopItems");

            HasKey(m => m.Id);

            Property(m => m.SliderId).IsRequired();
                 
            //navigation
            this.HasRequired(i => i.Slider)
                .WithMany(s => s.Items)
                .HasForeignKey(i => i.SliderId);          
        }
    }
}