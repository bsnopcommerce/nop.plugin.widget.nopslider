﻿using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Model;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Services.Events;
using Nop.Services.Media;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial class SliderService : ISliderService
    {   
        #region Fields

        private readonly IRepository<Slider> _sliderRepository;
        private IEventPublisher _eventPublisher;
        private ICacheManager _cacheService;
        private IPictureService _pictureService;

        #endregion

        #region Ctor
        public SliderService(IRepository<Slider> sliderRepository, IEventPublisher eventPublisher,
            ICacheManager cacheManager, IPictureService pictureService)
        {
            this._cacheService = cacheManager;
            this._sliderRepository = sliderRepository;
            _eventPublisher = eventPublisher;
            _pictureService = pictureService;
        }
        #endregion

        #region Methods

        #region Admin
        public virtual Slider GetSliderById(int? sliderId)
        {
            if (sliderId == 0)
                return null;

            return _sliderRepository.GetById(sliderId);
        }
        public virtual List<Slider> GetAllSliders()
        {
            return _sliderRepository.Table.ToList();
        }

        #region Banner
        public virtual void InsertBanner(Slider banner)
        {
            if (banner == null)
                throw new ArgumentNullException("slider");

            banner.SliderType = SliderType.Banner;

            _sliderRepository.Insert(banner);
            _eventPublisher.EntityInserted(banner);
        }
        public virtual void UpdateBanner(Slider banner)
        {
            if (banner == null)
                throw new ArgumentNullException("slider");

            var slider = GetSliderById(banner.Id);

            slider.SliderName = banner.SliderName;
            slider.Interval = banner.Interval;
            slider.IsActive = banner.IsActive;
            slider.SliderType = SliderType.Banner;
            slider.ZoneName = banner.ZoneName;
            slider.IsNavigationButton = banner.IsNavigationButton;
            if (slider.IsNavigationButton)
            {
                slider.NavigationButtonPosition = banner.NavigationButtonPosition;
            }
            else
                slider.NavigationButtonPosition = null;
            slider.IsProgressBar = banner.IsProgressBar;
            if (slider.IsProgressBar)
            {
                slider.ProgressBarPostition = banner.ProgressBarPostition;
            }
            else
                slider.ProgressBarPostition = null;
            slider.TransitionStyle = banner.TransitionStyle;
            slider.IconChevronActive = banner.IconChevronActive;
            slider.IsMouseDragOn = banner.IsMouseDragOn;

            _sliderRepository.Update(slider);
            _cacheService.Clear();
            _eventPublisher.EntityUpdated(slider);
        }
        #endregion

        #region CategoryBanner
        public virtual void InsertCategoryBanner(Slider categoryBanner)
        {
            if (categoryBanner == null)
                throw new ArgumentNullException("slider");

            categoryBanner.SliderType = SliderType.CategoryBanner;

            _sliderRepository.Insert(categoryBanner);
            _eventPublisher.EntityInserted(categoryBanner);
        }
        public virtual void UpdateCategoryBanner(Slider categoryBanner)
        {
            if (categoryBanner == null)
                throw new ArgumentNullException("slider");

            var slider = GetSliderById(categoryBanner.Id);

            slider.SliderName = categoryBanner.SliderName;
            slider.Interval = categoryBanner.Interval;
            slider.IsActive = categoryBanner.IsActive;
            slider.SliderType = SliderType.CategoryBanner;
            slider.ZoneName = "categorydetails_top";
            slider.IsNavigationButton = categoryBanner.IsNavigationButton;
            if (slider.IsNavigationButton)
            {
                slider.NavigationButtonPosition = categoryBanner.NavigationButtonPosition;
            }
            else
                slider.NavigationButtonPosition = null;
            slider.IsProgressBar = categoryBanner.IsProgressBar;
            if (slider.IsProgressBar)
            {
                slider.ProgressBarPostition = categoryBanner.ProgressBarPostition;
            }
            else
                slider.ProgressBarPostition = null;
            slider.TransitionStyle = categoryBanner.TransitionStyle;
            slider.IconChevronActive = categoryBanner.IconChevronActive;
            slider.IsMouseDragOn = categoryBanner.IsMouseDragOn;

            _sliderRepository.Update(slider);
            _cacheService.Clear();
            _eventPublisher.EntityUpdated(slider);


            _sliderRepository.Update(categoryBanner);
            _cacheService.Clear();
            _eventPublisher.EntityUpdated(categoryBanner);
        }
        #endregion

        #region Carousel
        public virtual void InsertCarousel(Slider carousel)
        {
            if (carousel == null)
                throw new ArgumentNullException("slider");

            carousel.SliderType = SliderType.Carousel;
            _sliderRepository.Insert(carousel);
            _eventPublisher.EntityInserted(carousel);
        }
        public virtual void UpdateCarousel(Slider carousel)
        {
            if (carousel == null)
                throw new ArgumentNullException("slider");

            Slider slider = GetSliderById(carousel.Id);

            slider.SliderName = carousel.SliderName;
            slider.Interval = carousel.Interval;
            slider.IsActive = carousel.IsActive;
            slider.SliderType = SliderType.Carousel;
            slider.ZoneName = carousel.ZoneName;
            slider.IconChevronActive = carousel.IconChevronActive;
            slider.NoOfItems = carousel.NoOfItems;
            slider.IsNavigationButton = carousel.IsNavigationButton;
            slider.NavigationButtonPosition = carousel.NavigationButtonPosition;
            slider.IsLazyLoad = carousel.IsLazyLoad;
            _sliderRepository.Update(slider);
            _cacheService.Clear();
            _eventPublisher.EntityUpdated(carousel);
        }
        #endregion

        #region DirectCarousel
        public virtual void InsertDirectCarousel(Slider directCarousel)
        {
            if (directCarousel == null)
                throw new ArgumentNullException("slider");

            directCarousel.SliderType = SliderType.DirectCarousel;

            _sliderRepository.Insert(directCarousel);
            _eventPublisher.EntityInserted(directCarousel);
        }
        public virtual void UpdateDirectCarousel(Slider directCarousel)
        {
            if (directCarousel == null)
                throw new ArgumentNullException("slider");

            Slider slider = GetSliderById(directCarousel.Id);

            slider.SliderName = directCarousel.SliderName;
            slider.Interval = directCarousel.Interval;
            slider.IsActive = directCarousel.IsActive;
            slider.SliderType = SliderType.DirectCarousel;
            slider.IconChevronActive = directCarousel.IconChevronActive;
            slider.NoOfItems = directCarousel.NoOfItems;
            slider.IsNavigationButton = directCarousel.IsNavigationButton;
            if (slider.IsNavigationButton)
            {
                slider.NavigationButtonPosition = directCarousel.NavigationButtonPosition;
            }
            else
                slider.NavigationButtonPosition = null;
            slider.IsLazyLoad = directCarousel.IsLazyLoad;
            _sliderRepository.Update(slider);
            _cacheService.Clear();
            _eventPublisher.EntityUpdated(directCarousel);
        }
        #endregion
        
        public virtual void UpdateSlider(Slider sliderUpdate)
        {
            var slider = GetSliderById(sliderUpdate.Id);

            slider.IsActive = sliderUpdate.IsActive;
            slider.IconChevronActive = sliderUpdate.IconChevronActive;
            slider.DisplayOrder = sliderUpdate.DisplayOrder;

            _sliderRepository.Update(slider);
        }
        public virtual void DeleteSlider(int sliderId)
        {
            var slider = GetSliderById(sliderId);
            _sliderRepository.Delete(slider);
        }
        #endregion

        #region Store
        public virtual List<Slider> SliderBannerWidget(string widgetZone)
        {
            return _sliderRepository.Table.Where(x => x.ZoneName == widgetZone && x.IsActive)
                    .OrderBy(x => x.DisplayOrder)
                    .ToList();
        }
        public virtual List<Slider> SliderCategoryBannerWidget(string widgetZone, int categoryId)
        {
            return _sliderRepository.Table.Where(x => x.ZoneName == widgetZone
            && x.IsActive && x.CategoryId == categoryId)
                    .OrderBy(x => x.DisplayOrder)
                    .ToList();
        }
        public virtual List<DirectCarouselModel> DirectSliderWidget()
        {
            var sliders = _sliderRepository.Table.
                Where(x => x.IsActive == true && x.SliderType == SliderType.DirectCarousel)
                .ToList();
            var models = new List<DirectCarouselModel>();
            foreach (var cl in sliders)
            {
                var model = new DirectCarouselModel
                {
                    Id = cl.Id,
                    SliderName = cl.SliderName,
                    NoOfItems = cl.NoOfItems,
                    IsNavigationButton = cl.IsNavigationButton,
                    NavigationButtonPosition = cl.NavigationButtonPosition,
                    IconChevronActive = cl.IconChevronActive,
                    IsLazyLoad = cl.IsLazyLoad
                };
                models.Add(model);
            }
            return models;
        }         
        #endregion

        #endregion
    }
}
