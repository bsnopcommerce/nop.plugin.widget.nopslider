﻿using Nop.Plugin.Widget.NopSlider.Domain;
using System.Collections.Generic;
using System.Linq;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial interface IItemService
    {
        ICollection<Item> GetSliderItems(int? SliderId);
        IOrderedEnumerable<Item> GetSliderItemsOrderbyDisplayOrder(int SliderId);
        void InsertItem(Item item);
        void UpdateItem(Item itemUpdate);
        void InsertNopItemCarousel(Item item);
        void DeleteItem(int ItemId);
    }
}
