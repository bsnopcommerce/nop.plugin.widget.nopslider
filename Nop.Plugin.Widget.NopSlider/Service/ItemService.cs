﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Services.Events;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Services.Media;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial class ItemService : IItemService
    {
        #region Fields
        private ICacheManager _cacheService;
        private IEventPublisher _eventPublisher;
        private IRepository<Item> _itemRepository;
        private IPictureService _pictureService;
        private IRepository<Slider> _sliderRepository;
        private ISliderService _sliderService;
        #endregion

        #region Constructor
        public ItemService(IRepository<Slider> sliderRepository,
            IRepository<Item> itemRepository, ISliderService sliderService,
            IEventPublisher eventPublisher, ICacheManager cacheManager, IPictureService pictureService)
        {
            _itemRepository = itemRepository;
            this._cacheService = cacheManager;
            this._sliderRepository = sliderRepository;
            _sliderService = sliderService;
            _eventPublisher = eventPublisher;
            _pictureService = pictureService;
        }
        #endregion

        #region Methods
        public virtual ICollection<Item> GetSliderItems(int? SliderId)
        {
            var slider = _sliderService.GetSliderById(SliderId);
            return slider.Items;
        }
        public virtual IOrderedEnumerable<Item> GetSliderItemsOrderbyDisplayOrder(int SliderId)
        {
            return _sliderService.GetSliderById(SliderId).Items.OrderBy(x => x.DisplayOrder);
        }
        public virtual void InsertItem(Item item)
        {
            var slider = _sliderService.GetSliderById(item.SliderId);

            if (slider.SliderType == SliderType.Banner)
            {
                item.ItemType = ItemType.Banner;
            }
            else
                item.ItemType = ItemType.CategoryBanner;

            item.FilePath = _pictureService.GetPictureUrl(item.PictureId);

            slider.Items.Add(item);
            _sliderRepository.Update(slider);
        }
        public void InsertNopItemCarousel(Item item)
        {
            var slider = _sliderService.GetSliderById(item.SliderId);

            item.ItemType = ItemType.Carousel;
            slider.Items.Add(item);
            _sliderRepository.Update(slider);
        }
        public virtual void UpdateItem(Item itemUpdate)
        {
            Item item = _itemRepository.GetById(itemUpdate.PictureId);

            item.Caption = itemUpdate.Caption;
            item.Url = itemUpdate.Url;
            item.DisplayOrder = itemUpdate.DisplayOrder;
            _itemRepository.Update(item);
        }
        public void DeleteItem(int ItemId)
        {
            Item item = _itemRepository.GetById(ItemId);
            _itemRepository.Delete(item);
        } 
        #endregion

    }
}
