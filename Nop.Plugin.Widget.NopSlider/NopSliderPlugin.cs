﻿using Nop.Core.Data;
using Nop.Core.Plugins;
using Nop.Plugin.Widgets.NopSlider.Data;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Services.Cms;
using Nop.Web.Framework.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Routing;
using Nop.Services.Localization;

namespace Nop.Plugin.Widgets.NopSlider
{
    public class NopSliderPlugin : BasePlugin, IWidgetPlugin, IAdminMenuPlugin
    {
        private SliderObjectContext _context;
        private IRepository<Slider> _sliderRepo;
        public NopSliderPlugin(SliderObjectContext context, IRepository<Slider> sliderRepo)
        {
            _context = context;
            _sliderRepo = sliderRepo;
        }

        public override void Install()
        {
            _context.Install();
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.CreateitemMessage", "Please create and save slider before creating an item");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.SliderName", "Name");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.SliderName.Hint", "The Name of your plugin");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.interval", "Interval");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.interval.Hint", "Interval");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.IsActive", "Is Active");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.IsActive.Hint", "Is Active");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover", "Pause On Hover");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover.Hint", "Pause On Hover");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.Wrap", "Wrap");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.Wrap.Hint", "Wrap");

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.ZoneName", "ZoneName");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.ZoneName.Hint", "ZoneName");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NoOfItems", "Number of Items");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NoOfItems.Hint", "Number of items per page");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.CategoryId", "Category");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.CategoryId.Hint", "Banner Category");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsNavigationButton", "Is NavigationButton");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsNavigationButton.Hint", "Is NavigationButton");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NavigationButtonPosition", "Navigation Button Position");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.NavigationButtonPosition.Hint", "Navigation Button Position");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsProgressBar", "Is Progress Bar");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsProgressBar.Hint", "Is Progress Bar");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.ProgressBarPostition", "Progress Bar Postition");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.ProgressBarPostition.Hint", "Progress Bar Postition");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.TransitionStyle", "Transition Style");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.TransitionStyle.Hint", "Transition Style");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IconChevronActive", "Icon Chevron Active");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IconChevronActive.Hint", "Icon Chevron Active");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsMouseDragOn", "Is Mouse DragOn");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsMouseDragOn.Hint", "Is Mouse DragOn");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.DisplayOrder", "Display Order");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.DisplayOrder.Hint", "Display Order");

            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsLazyLoad", "Is Lazy Load");
            this.AddOrUpdatePluginLocaleResource("Plugins.Misc.NopSlider.IsLazyLoad.Hint", "Is Lazy Load");

 

            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.SliderNameRequired", "Please Provide a Slider Name");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.SliderIntervalRequired", "Interval must be greater than 0");
            this.AddOrUpdatePluginLocaleResource("plugins.misc.Nopslider.GreaterThanZero", "Interval must between 1 and 10");

            base.Install();
        }

        public override void Uninstall()
        {
            _context.Uninstall();
            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.CreateItemMessage");

            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.SliderName");
            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.SliderName.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.interval");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.interval.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.IsActive");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.IsActive.Hint");

            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.CategoryId");
            this.DeletePluginLocaleResource("Plugins.Misc.NopSlider.CategoryId.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.PauseOnHover.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.Wrap");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.Wrap.Hint");

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.ZoneName");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.ZoneName.Hint");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.NoOfItems");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.NoOfItems.Hint");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsNavigationButton");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsNavigationButton");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsProgressBar");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsProgressBar");


            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.ProgressBarPostition");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.ProgressBarPostition");


            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.TransitionStyle");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.TransitionStyle");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IconChevronActive");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IconChevronActive");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsMouseDragOn");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.IsMouseDragOn");

            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.isProduct");
            this.DeletePluginLocaleResource("plugins.Misc.NopSlider.isProduct"); 

            this.DeletePluginLocaleResource("plugins.misc.Nopslider.SliderNameRequired");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.SliderIntervalRequired");
            this.DeletePluginLocaleResource("plugins.misc.Nopslider.GreaterThanZero");

            base.Uninstall();
        }

        public void GetConfigurationRoute(out string actionName, out string controllerName, out System.Web.Routing.RouteValueDictionary routeValues)
        {
            actionName = "Configuration";
            controllerName = "Slider";
            routeValues = new RouteValueDictionary(){
                { "Namespaces", "Nop.Plugin.Widget.Nopslider.Controller"},
                { "area", null}
            };
        }

        public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out System.Web.Routing.RouteValueDictionary routeValues)

        {
            if (widgetZone == "footer")
            {
                actionName = "DirectCarouselWidget";
                controllerName = "Slider";
                routeValues = new RouteValueDictionary(){
                { "Namespaces", "Nop.Plugin.Widget.Nopslider.Controller"},
                { "area", null},
                { "widgetZone",widgetZone}
            };
            }
            else if (widgetZone == "categorydetails_top")
            {
                actionName = "CategoryBannerWidget";
                controllerName = "Slider";
                routeValues = new RouteValueDictionary(){
                { "Namespaces", "Nop.Plugin.Widget.Nopslider.Controller"},
                { "area", null},
                { "widgetZone",widgetZone }
            };
            }
            else
            {
                actionName = "SliderWidget";
                controllerName = "Slider";
                routeValues = new RouteValueDictionary(){
                { "Namespaces", "Nop.Plugin.Widget.Nopslider.Controller"},
                { "area", null},
                { "widgetZone",widgetZone }
            };
            }            
        }

        public IList<string> GetWidgetZones()
        {
            var sliders = _sliderRepo.Table.Where(x => x.IsActive == true).ToList();
            var zoneNames = new List<string>();
            foreach (var slider in sliders)
            {
                zoneNames.Add(slider.ZoneName);
            }
            zoneNames.Add("footer");
            return zoneNames;
        }

        public bool Authenticate()
        {
            return true;
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                Visible = true,
                Title = "Nop Slider",
                Url = "/Slider/CreateUpdateNopSlider"
            };

            var manageSliders = new SiteMapNode()
            {
                Visible = true,
                Title = "Manage Sliders",
                Url = "/Slider/ManageSliders"
            };

            menuItem.ChildNodes.Add(manageSliders);
            rootNode.ChildNodes.Add(menuItem);
        }
    }

}
