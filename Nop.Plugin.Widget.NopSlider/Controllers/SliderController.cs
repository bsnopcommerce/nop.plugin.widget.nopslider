﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Media;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Model;
using Nop.Plugin.Widgets.NopSlider.Service;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Services.Seo;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Extensions;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using Nop.Web.Models.Catalog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.NopSlider.Controllers
{
    [AdminAuthorize]
    public class SliderController : BasePluginController
    {
        #region Fields
        private IRepository<Slider> _sliderRepo;
        private ISliderService _sliderService;
        private IRepository<Item> _itemRepo;
        private IPictureService _pictureService;
        private ICacheManager _cacheService;
        private readonly IProductService _productService;
        private readonly IProductTemplateService _productTemplateService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly ICustomerService _customerService;
        private readonly IUrlRecordService _urlRecordService;
        private readonly IWorkContext _workContext;
        private readonly ILanguageService _languageService;
        private readonly ILocalizationService _localizationService;
        private readonly ILocalizedEntityService _localizedEntityService;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly ITaxCategoryService _taxCategoryService;
        private readonly IProductTagService _productTagService;
        private readonly ICopyProductService _copyProductService;
        private readonly IPdfService _pdfService;
        private readonly IExportManager _exportManager;
        private readonly IImportManager _importManager;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        private readonly IAclService _aclService;
        private readonly IStoreService _storeService;
        private readonly IOrderService _orderService;
        private readonly IStoreMappingService _storeMappingService;
        private readonly IVendorService _vendorService;
        private readonly IShippingService _shippingService;
        private readonly IShipmentService _shipmentService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;
        private readonly IMeasureService _measureService;
        private readonly MeasureSettings _measureSettings;
        private readonly AdminAreaSettings _adminAreaSettings;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IDiscountService _discountService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IBackInStockSubscriptionService _backInStockSubscriptionService;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IDownloadService _downloadService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IStoreContext _storeContext;
        private readonly IPriceFormatter _priceFormatter;
        private readonly ITaxService _taxService;
        private readonly IWebHelper _webHelper;
        private readonly ICacheManager _cacheManager;
        private readonly MediaSettings _mediaSettings;
        private readonly CatalogSettings _catalogSettings;
        private IItemService _itemService;

        #endregion

        #region Constructors
        public SliderController(IRepository<Slider> sliderRepo,
            ISliderService sliderService, IItemService itemService, IRepository<Item> itemRepo,
            ICacheManager cacheManager, IPictureService pictureService,
            IPermissionService permissionService,
            IProductService productService,
            IProductTemplateService productTemplateService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            ICustomerService customerService,
            IUrlRecordService urlRecordService,
            IWorkContext workContext,
            ILanguageService languageService,
            ILocalizationService localizationService,
            ILocalizedEntityService localizedEntityService,
            ISpecificationAttributeService specificationAttributeService,
            ITaxCategoryService taxCategoryService,
            IProductTagService productTagService,
            ICopyProductService copyProductService,
            IPdfService pdfService,
            IExportManager exportManager,
            IImportManager importManager,
            ICustomerActivityService customerActivityService,
            IAclService aclService,
            IStoreService storeService,
            IOrderService orderService,
            IStoreMappingService storeMappingService,
            IVendorService vendorService,
            IShippingService shippingService,
            IShipmentService shipmentService,
            ICurrencyService currencyService,
            CurrencySettings currencySettings,
            IMeasureService measureService,
            MeasureSettings measureSettings,
            AdminAreaSettings adminAreaSettings,
            IDateTimeHelper dateTimeHelper,
            IDiscountService discountService,
            IProductAttributeService productAttributeService,
            IBackInStockSubscriptionService backInStockSubscriptionService,
            IShoppingCartService shoppingCartService,
            IProductAttributeFormatter productAttributeFormatter,
            IProductAttributeParser productAttributeParser,
            IDownloadService downloadService,
            IStoreContext storeContext,
            IPriceCalculationService priceCalculationService,
            IPriceFormatter priceFormatter,
            ITaxService taxService,
            IWebHelper webHelper,
            MediaSettings mediaSettings,
            CatalogSettings catalogSettings
            )
        {
            this._cacheService = cacheManager;
            _pictureService = pictureService;
            _sliderRepo = sliderRepo;
            _sliderService = sliderService;
            _itemService = itemService;
            _itemRepo = itemRepo;
            this._permissionService = permissionService;
            this._productService = productService;
            this._productTemplateService = productTemplateService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._customerService = customerService;
            this._urlRecordService = urlRecordService;
            this._workContext = workContext;
            this._languageService = languageService;
            this._localizationService = localizationService;
            this._localizedEntityService = localizedEntityService;
            this._specificationAttributeService = specificationAttributeService;
            this._pictureService = pictureService;
            this._taxCategoryService = taxCategoryService;
            this._productTagService = productTagService;
            this._copyProductService = copyProductService;
            this._pdfService = pdfService;
            this._exportManager = exportManager;
            this._importManager = importManager;
            this._customerActivityService = customerActivityService;
            this._permissionService = permissionService;
            this._aclService = aclService;
            this._storeService = storeService;
            this._orderService = orderService;
            this._storeMappingService = storeMappingService;
            this._vendorService = vendorService;
            this._shippingService = shippingService;
            this._shipmentService = shipmentService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._measureService = measureService;
            this._measureSettings = measureSettings;
            this._adminAreaSettings = adminAreaSettings;
            this._dateTimeHelper = dateTimeHelper;
            this._discountService = discountService;
            this._productAttributeService = productAttributeService;
            this._backInStockSubscriptionService = backInStockSubscriptionService;
            this._shoppingCartService = shoppingCartService;
            this._productAttributeFormatter = productAttributeFormatter;
            this._productAttributeParser = productAttributeParser;
            this._downloadService = downloadService;
            this._priceCalculationService = priceCalculationService;
            this._taxService = taxService;
            this._priceFormatter = priceFormatter;
            this._webHelper = webHelper;
            this._mediaSettings = mediaSettings;
            this._catalogSettings = catalogSettings;
            this._storeContext = storeContext;
            this._cacheManager = cacheManager;
        }
        #endregion

        #region Utilities
        [NonAction]
        protected virtual IEnumerable<ProductOverviewModel> PrepareProductOverviewModels(IEnumerable<Product> products, int IsLazyLoad, int NoOfItems,
            bool preparePriceModel = true, bool preparePictureModel = true,
            int? productThumbPictureSize = null, bool prepareSpecificationAttributes = false,
            bool forceRedirectionAfterAddingToCart = false)
        {
            if (IsLazyLoad == 1)
            {
                ViewBag.IsLazyLoad = 1;
            }
            else
            {
                ViewBag.IsLazyLoad = 0;
            }
            if (NoOfItems != 3)
            {
                ViewBag.items = 1;
            }
            else
            {
                ViewBag.items = 0;
            }

            return this.PrepareProductOverviewModels(_workContext,
                _storeContext, _categoryService, _productService, _specificationAttributeService,
                _priceCalculationService, _priceFormatter, _permissionService,
                _localizationService, _taxService, _currencyService,
                _pictureService, _webHelper, _cacheManager,
                _catalogSettings, _mediaSettings, products,
                preparePriceModel, preparePictureModel,
                productThumbPictureSize, prepareSpecificationAttributes,
                forceRedirectionAfterAddingToCart);
        }

        [NonAction]
        protected virtual List<int> GetChildCategoryIds(int parentCategoryId)
        {
            var categoriesIds = new List<int>();
            var categories = _categoryService.GetAllCategoriesByParentCategoryId(parentCategoryId, true);
            foreach (var category in categories)
            {
                categoriesIds.Add(category.Id);
                categoriesIds.AddRange(GetChildCategoryIds(category.Id));
            }
            return categoriesIds;
        }

        #region Mappings

        //Slider
        [NonAction]
        public static void PrepareSliderModel(SliderModel sliderModel, Slider sliderEntity)
        {
            if (sliderModel == null)
                throw new ArgumentNullException("model");
            if (sliderModel == null)
                throw new ArgumentNullException("entity");

            sliderModel.Id = sliderEntity.Id;
            sliderModel.SliderName = sliderEntity.SliderName;
            sliderModel.IsActive = sliderEntity.IsActive;
            sliderModel.SliderType = sliderEntity.SliderType;
            sliderModel.SliderTypeName = sliderEntity.SliderTypeName;
            sliderModel.IconChevronActive = sliderEntity.IconChevronActive;
            sliderModel.DisplayOrder = sliderEntity.DisplayOrder;
        }

        [NonAction]
        public static Slider SliderModelToSliderEntity(SliderModel sliderModel)
        {
            if (sliderModel == null)
                return null;

            var sliderEntity = new Slider();
            return SliderModelToSliderEntity(sliderModel, sliderEntity);
        }

        [NonAction]
        public static Slider SliderModelToSliderEntity(SliderModel sliderModel, Slider sliderEntity)
        {
            if (sliderModel == null)
                return sliderEntity;
            sliderEntity.Id = sliderModel.Id;
            sliderEntity.SliderName = sliderModel.SliderName;
            sliderEntity.IsActive = sliderModel.IsActive;
            sliderEntity.SliderType = sliderModel.SliderType;
            sliderEntity.SliderTypeName = sliderModel.SliderTypeName;
            sliderEntity.IconChevronActive = sliderModel.IconChevronActive;
            sliderEntity.DisplayOrder = sliderModel.DisplayOrder;

            return sliderEntity;
        }

        //Banner
        [NonAction]
        public static void PrepareBannerModel(BannerModel bannerModel, Slider sliderEntity)
        {
            if (bannerModel == null)
                throw new ArgumentNullException("model");
            if (bannerModel == null)
                throw new ArgumentNullException("entity");

            bannerModel.Id = sliderEntity.Id;
            bannerModel.SliderName = sliderEntity.SliderName;
            bannerModel.IsActive = sliderEntity.IsActive;
            bannerModel.ZoneName = sliderEntity.ZoneName;
            bannerModel.Interval = sliderEntity.Interval;
            bannerModel.NoOfItems = sliderEntity.NoOfItems;
            bannerModel.IsNavigationButton = sliderEntity.IsNavigationButton;
            bannerModel.NavigationButtonPosition = sliderEntity.NavigationButtonPosition;
            bannerModel.SliderType = sliderEntity.SliderType;
            bannerModel.IsProgressBar = sliderEntity.IsProgressBar;
            bannerModel.ProgressBarPostition = sliderEntity.ProgressBarPostition;
            bannerModel.TransitionStyle = sliderEntity.TransitionStyle;            
            bannerModel.IconChevronActive = sliderEntity.IconChevronActive;
            bannerModel.IsMouseDragOn = sliderEntity.IsMouseDragOn;
            foreach(var item in sliderEntity.Items)
            {
                ItemModel model = new ItemModel();
                PrepareItemModel(model, item);
                bannerModel.Items.Add(model);
            }           
        }

        [NonAction]
        public static Slider BannerModelToSliderEntity(BannerModel bannerModel)
        {
            if (bannerModel == null)
                return null;

            var sliderEntity = new Slider();
            return BannerModelToSliderEntity(bannerModel, sliderEntity);
        }

        [NonAction]
        public static Slider BannerModelToSliderEntity(BannerModel bannerModel, Slider sliderEntity)
        {
            if (bannerModel == null)
                return sliderEntity;
            sliderEntity.Id = bannerModel.Id;
            sliderEntity.SliderName = bannerModel.SliderName;
            sliderEntity.IsActive = bannerModel.IsActive;
            sliderEntity.ZoneName = bannerModel.ZoneName;
            sliderEntity.Interval = bannerModel.Interval;
            sliderEntity.NoOfItems = bannerModel.NoOfItems;
            sliderEntity.IsNavigationButton = bannerModel.IsNavigationButton;
            sliderEntity.NavigationButtonPosition = bannerModel.NavigationButtonPosition;
            sliderEntity.SliderType = bannerModel.SliderType;
            sliderEntity.IsProgressBar = bannerModel.IsProgressBar;
            sliderEntity.ProgressBarPostition = bannerModel.ProgressBarPostition;
            sliderEntity.TransitionStyle = bannerModel.TransitionStyle;
            sliderEntity.IconChevronActive = bannerModel.IconChevronActive;
            sliderEntity.IsMouseDragOn = bannerModel.IsMouseDragOn;

            return sliderEntity;
        }

        //CategoryBanner
        [NonAction]
        public static void PrepareCategoryBannerModel(CategoryBannerModel categoryBannerModel, Slider sliderEntity)
        {
            if (categoryBannerModel == null)
                throw new ArgumentNullException("model");
            if (categoryBannerModel == null)
                throw new ArgumentNullException("entity");

            categoryBannerModel.Id = sliderEntity.Id;
            categoryBannerModel.SliderName = sliderEntity.SliderName;
            categoryBannerModel.IsActive = sliderEntity.IsActive;
            categoryBannerModel.NoOfItems = sliderEntity.NoOfItems;
            categoryBannerModel.Interval = sliderEntity.Interval;
            categoryBannerModel.CategoryId = sliderEntity.CategoryId;
            categoryBannerModel.ZoneName = sliderEntity.ZoneName;
            categoryBannerModel.SliderType = sliderEntity.SliderType;
            categoryBannerModel.IsNavigationButton = sliderEntity.IsNavigationButton;
            categoryBannerModel.NavigationButtonPosition = sliderEntity.NavigationButtonPosition;
            categoryBannerModel.IsProgressBar = sliderEntity.IsProgressBar;
            categoryBannerModel.ProgressBarPostition = sliderEntity.ProgressBarPostition;
            categoryBannerModel.TransitionStyle = sliderEntity.TransitionStyle;
            categoryBannerModel.IconChevronActive = sliderEntity.IconChevronActive;
            categoryBannerModel.IsMouseDragOn = sliderEntity.IsMouseDragOn;

            foreach (var item in sliderEntity.Items)
            {
                ItemModel model = new ItemModel();
                PrepareItemModel(model, item);
                categoryBannerModel.Items.Add(model);
            }
        }

        [NonAction]
        public static Slider CategoryBannerModelToSliderEntity(CategoryBannerModel categoryBannerModel)
        {
            if (categoryBannerModel == null)
                return null;

            var sliderEntity = new Slider();
            return CategoryBannerModelToSliderEntity(categoryBannerModel, sliderEntity);
        }

        [NonAction]
        public static Slider CategoryBannerModelToSliderEntity(CategoryBannerModel categoryBannerModel, Slider sliderEntity)
        {
            if (categoryBannerModel == null)
                return sliderEntity;

            sliderEntity.Id = categoryBannerModel.Id;
            sliderEntity.SliderName = categoryBannerModel.SliderName;
            sliderEntity.IsActive = categoryBannerModel.IsActive;
            sliderEntity.NoOfItems = categoryBannerModel.NoOfItems;
            sliderEntity.Interval = categoryBannerModel.Interval;
            sliderEntity.CategoryId = categoryBannerModel.CategoryId;
            sliderEntity.ZoneName = categoryBannerModel.ZoneName;
            sliderEntity.SliderType = categoryBannerModel.SliderType;
            sliderEntity.IsNavigationButton = categoryBannerModel.IsNavigationButton;
            sliderEntity.NavigationButtonPosition = categoryBannerModel.NavigationButtonPosition;
            sliderEntity.IsProgressBar = categoryBannerModel.IsProgressBar;
            sliderEntity.ProgressBarPostition = categoryBannerModel.ProgressBarPostition;
            sliderEntity.TransitionStyle = categoryBannerModel.TransitionStyle;
            sliderEntity.IconChevronActive = categoryBannerModel.IconChevronActive;
            sliderEntity.IsMouseDragOn = categoryBannerModel.IsMouseDragOn;

            return sliderEntity;
        }

        //Carousel
        [NonAction]
        public static void PrepareCarouselModel(CarouselModel carouselModel, Slider sliderEntity)
        {
            if (carouselModel == null)
                throw new ArgumentNullException("model");
            if (carouselModel == null)
                throw new ArgumentNullException("entity");

            carouselModel.Id = sliderEntity.Id;
            carouselModel.SliderName = sliderEntity.SliderName;
            carouselModel.IsActive = sliderEntity.IsActive;
            carouselModel.ZoneName = sliderEntity.ZoneName;
            carouselModel.Interval = sliderEntity.Interval;
            carouselModel.NoOfItems = sliderEntity.NoOfItems;
            carouselModel.IsNavigationButton = sliderEntity.IsNavigationButton;
            carouselModel.NavigationButtonPosition = sliderEntity.NavigationButtonPosition;
            carouselModel.SliderType = sliderEntity.SliderType;
            carouselModel.IconChevronActive = sliderEntity.IconChevronActive;
            carouselModel.IsMouseDragOn = sliderEntity.IsMouseDragOn;
            carouselModel.isProduct = sliderEntity.isProduct;
            carouselModel.DisplayOrder = sliderEntity.DisplayOrder;
            carouselModel.IsLazyLoad = sliderEntity.IsLazyLoad;
        }

        [NonAction]
        public static Slider CarouselModelToSliderEntity(CarouselModel carouselModel)
        {
            if (carouselModel == null)
                return null;

            var sliderEntity = new Slider();
            return CarouselModelToSliderEntity(carouselModel, sliderEntity);
        }

        [NonAction]
        public static Slider CarouselModelToSliderEntity(CarouselModel carouselModel, Slider sliderEntity)
        {
            if (carouselModel == null)
                return sliderEntity;
            sliderEntity.Id = carouselModel.Id;
            sliderEntity.SliderName = carouselModel.SliderName;
            sliderEntity.IsActive = carouselModel.IsActive;
            sliderEntity.ZoneName = carouselModel.ZoneName;
            sliderEntity.Interval = carouselModel.Interval;
            sliderEntity.NoOfItems = carouselModel.NoOfItems;
            sliderEntity.IsNavigationButton = carouselModel.IsNavigationButton;
            sliderEntity.NavigationButtonPosition = carouselModel.NavigationButtonPosition;
            sliderEntity.SliderType = carouselModel.SliderType;
            sliderEntity.IconChevronActive = carouselModel.IconChevronActive;
            sliderEntity.IsMouseDragOn = carouselModel.IsMouseDragOn;
            sliderEntity.isProduct = carouselModel.isProduct;
            sliderEntity.DisplayOrder = carouselModel.DisplayOrder;
            sliderEntity.IsLazyLoad = carouselModel.IsLazyLoad;

            return sliderEntity;
        }

        //DirectCarousel
        [NonAction]
        public static void PrepareDirectCarouselModel(DirectCarouselModel directCarouselModel, Slider sliderEntity)
        {
            if (directCarouselModel == null)
                throw new ArgumentNullException("model");
            if (directCarouselModel == null)
                throw new ArgumentNullException("entity");

            directCarouselModel.Id = sliderEntity.Id;
            directCarouselModel.SliderName = sliderEntity.SliderName;
            directCarouselModel.IsActive = sliderEntity.IsActive;
            directCarouselModel.NoOfItems = sliderEntity.NoOfItems;
            directCarouselModel.IsNavigationButton = sliderEntity.IsNavigationButton;
            directCarouselModel.NavigationButtonPosition = sliderEntity.NavigationButtonPosition;
            directCarouselModel.IconChevronActive = sliderEntity.IconChevronActive;
            directCarouselModel.IsLazyLoad = sliderEntity.IsLazyLoad;
        }

        [NonAction]
        public static Slider DirectCarouselModelToSliderEntity(DirectCarouselModel directCarouselModel)
        {
            if (directCarouselModel == null)
                return null;

            var sliderEntity = new Slider();
            return DirectCarouselModelToSliderEntity(directCarouselModel, sliderEntity);
        }

        [NonAction]
        public static Slider DirectCarouselModelToSliderEntity(DirectCarouselModel directCarouselModel, Slider sliderEntity)
        {
            if (directCarouselModel == null)
                return sliderEntity;

            sliderEntity.Id = directCarouselModel.Id;
            sliderEntity.SliderName = directCarouselModel.SliderName;
            sliderEntity.IsActive = directCarouselModel.IsActive;
            sliderEntity.NoOfItems = directCarouselModel.NoOfItems;
            sliderEntity.IsNavigationButton = directCarouselModel.IsNavigationButton;
            sliderEntity.NavigationButtonPosition = directCarouselModel.NavigationButtonPosition;
            sliderEntity.IconChevronActive = directCarouselModel.IconChevronActive;
            sliderEntity.IsLazyLoad = directCarouselModel.IsLazyLoad;

            return sliderEntity;
        }

        //Item
        [NonAction]
        public static void PrepareItemModel(ItemModel itemModel, Item itemEntity)
        {
            if (itemModel == null)
                throw new ArgumentNullException("model");
            if (itemModel == null)
                throw new ArgumentNullException("entity");

            itemModel.Id = itemEntity.Id;
            itemModel.PictureId = itemEntity.PictureId;
            itemModel.EntityId = itemEntity.EntityId;
            itemModel.SliderId = itemEntity.SliderId;
            itemModel.Caption = itemEntity.Caption;
            itemModel.Url = itemEntity.Url;
            itemModel.FilePath = itemEntity.FilePath;
            itemModel.DisplayOrder = itemEntity.DisplayOrder;
            itemModel.ItemType = itemEntity.ItemType;
            itemModel.Slider = new SliderModel();
            PrepareSliderModel(itemModel.Slider, itemEntity.Slider);
        }

        [NonAction]
        public static Item ItemModelToItemEntity(ItemModel itemModel)
        {
            if (itemModel == null)
                return null;

            var itemEntity = new Item();
            return ItemModelToItemEntity(itemModel, itemEntity);
        }

        [NonAction]
        public static Item ItemModelToItemEntity(ItemModel itemModel, Item itemEntity)
        {
            if (itemModel == null)
                return itemEntity;

            itemEntity.Id = itemModel.Id;
            itemEntity.PictureId = itemModel.PictureId;
            itemEntity.EntityId = itemModel.EntityId;
            itemEntity.SliderId = itemModel.SliderId;
            itemEntity.Caption = itemModel.Caption;
            itemEntity.Url = itemModel.Url;
            itemEntity.FilePath = itemModel.FilePath;
            itemEntity.DisplayOrder = itemModel.DisplayOrder;
            itemEntity.ItemType = itemModel.ItemType;
            itemEntity.Slider = SliderModelToSliderEntity(itemModel.Slider);

            return itemEntity;
        }

        #endregion

        #endregion

        #region Methods
        #region Admin

        #region Slider
        public ActionResult ManageSliders()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ListSliders()
        {
            var sliders = _sliderService.GetAllSliders();

            var gridModel = new DataSourceResult
            {
                Data = sliders.Select(x => new SliderModel
                {
                    Id = x.Id,
                    SliderName = x.SliderName,
                    IsActive = x.IsActive,
                    SliderType = x.SliderType,
                    SliderTypeName = x.SliderType.ToString(),
                    IconChevronActive = x.IconChevronActive,
                    DisplayOrder = x.DisplayOrder
                }),
                Total = sliders.Count()
            };
            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult UpdateSlider(Slider sliderUpdate)
        {
            _sliderService.UpdateSlider(sliderUpdate);
            return new NullJsonResult();
        }

        [HttpPost]
        public ActionResult DeleteSlider(int Id)
        {
            _sliderService.DeleteSlider(Id);

            return new NullJsonResult();
        }

        #region Banner

        public ActionResult CreateBanner(int SliderId = 0)
        {
            Slider Slider = new Slider() { Interval = 3 };
            if (SliderId > 0)
            {
                Slider = _sliderService.GetSliderById(SliderId);
            }

            BannerModel BannerModel = new BannerModel();
            PrepareBannerModel(BannerModel, Slider);
            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateBanner.cshtml", BannerModel);
        }

        [HttpPost]
        public ActionResult CreateBanner(BannerModel bannerModel)
        {
            if (ModelState.IsValid)
            {
                var slider = BannerModelToSliderEntity(bannerModel);
                _sliderService.InsertBanner(slider);
                SuccessNotification("Slider Created Successfullly!");
                return RedirectToRoute(new
                {
                    Action = "CreateBanner",
                    Controller = "Slider",
                    SliderId = slider.Id
                });
            }
            else
            {
                return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateBanner.cshtml");
            }
        }

        public ActionResult UpdateBanner(int SliderId)
        {
            Slider Slider = new Slider();
            Slider = _sliderService.GetSliderById(SliderId);

            BannerModel BannerModel = new BannerModel();
            PrepareBannerModel(BannerModel, Slider);

            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateBanner.cshtml", BannerModel);
        }

        [HttpPost]
        public ActionResult UpdateBanner(BannerModel bannerModel)
        {
            var sliderEntity = BannerModelToSliderEntity(bannerModel);
            Slider slider = _sliderService.GetSliderById(sliderEntity.Id);
            _sliderService.UpdateBanner(sliderEntity);
            SuccessNotification("Changed Save!");
            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateBanner.cshtml", bannerModel);
        }
        #endregion

        #region CategoryBanner

        public ActionResult CreateCategoryBanner(int SliderId = 0)
        {
            Slider Slider = new Slider() { Interval = 3 };

            if (SliderId > 0)
            {
                Slider = _sliderService.GetSliderById(SliderId);
            }

            CategoryBannerModel CategoryBannerModel = new CategoryBannerModel();
            PrepareCategoryBannerModel(CategoryBannerModel, Slider);

            List<Category> categories = new List<Category>();
            var firstLevelCategories = _categoryService.GetAllCategoriesByParentCategoryId(0, false, false).ToList();
            categories.AddRange(firstLevelCategories);

            foreach (var firstLevelCategory in firstLevelCategories)
            {
                var secondLavelCategories = _categoryService.GetAllCategoriesByParentCategoryId(firstLevelCategory.Id, false, false).ToList();
                categories.AddRange(secondLavelCategories);
            }           
            
            foreach (var c in categories)
                CategoryBannerModel.CategoryList.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCategoryBanner.cshtml", CategoryBannerModel);
        }

        [HttpPost]
        public ActionResult CreateCategoryBanner(CategoryBannerModel categoryBannerModel)
        {
            if (ModelState.IsValid)
            {
                var slider = CategoryBannerModelToSliderEntity(categoryBannerModel);
                slider.ZoneName = "categorydetails_top";

                _sliderService.InsertCategoryBanner(slider);
                SuccessNotification("Slider Created Successfullly!");
                return RedirectToRoute(new
                {
                    Action = "CreateCategoryBanner",
                    Controller = "Slider",
                    SliderId = slider.Id
                });
            }
            else
            {
                return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCategoryBanner.cshtml");
            }
        }

        public ActionResult UpdateCategoryBanner(int SliderId)
        {
            Slider Slider = new Slider();
            Slider = _sliderService.GetSliderById(SliderId);

            CategoryBannerModel CategoryBannerModel = new CategoryBannerModel();
            PrepareCategoryBannerModel(CategoryBannerModel, Slider);

            List<Category> categories = new List<Category>();
            var firstLevelCategories = _categoryService.GetAllCategoriesByParentCategoryId(0, false, false).ToList();
            categories.AddRange(firstLevelCategories);

            foreach (var firstLevelCategory in firstLevelCategories)
            {
                var secondLavelCategories = _categoryService.GetAllCategoriesByParentCategoryId(firstLevelCategory.Id, false, false).ToList();
                categories.AddRange(secondLavelCategories);
            }
            foreach (var c in categories)
                CategoryBannerModel.CategoryList.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            
            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCategoryBanner.cshtml", CategoryBannerModel);
        }

        [HttpPost]
        public ActionResult UpdateCategoryBanner(CategoryBannerModel categoryBannerModel)
        {
            var sliderEntity = CategoryBannerModelToSliderEntity(categoryBannerModel);
            Slider slider = _sliderService.GetSliderById(sliderEntity.Id);
            _sliderService.UpdateCategoryBanner(sliderEntity);
            SuccessNotification("Changed Save!");
            return UpdateCategoryBanner(categoryBannerModel.Id);
        }

        #endregion

        #region Carousel

        public ActionResult CreateCarousel(int SliderId = 0)
        {
            Slider Slider = new Slider() { Interval = 3, NoOfItems = 3 };
            if (SliderId > 0)
            {
                Slider = _sliderService.GetSliderById(SliderId);
            }

            CarouselModel CarouselModel = new CarouselModel();
            PrepareCarouselModel(CarouselModel, Slider);

            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCarousel.cshtml", CarouselModel);
        }

        [HttpPost]
        public ActionResult CreateCarousel(CarouselModel carouselModel)
        {
            if (ModelState.IsValid)
            {
                var slider = CarouselModelToSliderEntity(carouselModel);
                _sliderService.InsertCarousel(slider);
                SuccessNotification("Slider Created Successfullly!");
                return RedirectToRoute(new
                {
                    Action = "CreateCarousel",
                    Controller = "Slider",
                    SliderId = slider.Id
                });
            }
            else
            {
                return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCarousel.cshtml");
            }
        }

        public ActionResult UpdateCarousel(int SliderId)
        {
            Slider Slider = new Slider();
            Slider = _sliderService.GetSliderById(SliderId);

            CarouselModel CarouselModel = new CarouselModel();
            PrepareCarouselModel(CarouselModel, Slider);

            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCarousel.cshtml", CarouselModel);
        }

        [HttpPost]
        public ActionResult UpdateCarousel(CarouselModel carouselModel)
        {
            var sliderEntity = CarouselModelToSliderEntity(carouselModel);
            Slider slider = _sliderService.GetSliderById(sliderEntity.Id);
            _sliderService.UpdateCarousel(sliderEntity);
            SuccessNotification("Changed Save!");
            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateCarousel.cshtml", carouselModel);
        }
        #endregion

        #region DirectSlider

        public ActionResult CreateDirectCarousel(int SliderId = 0)
        {
            Slider Slider = new Slider();
            if (SliderId > 0)
            {
                Slider = _sliderService.GetSliderById(SliderId);
            }

            DirectCarouselModel DirectCarouselModel = new DirectCarouselModel();
            PrepareDirectCarouselModel(DirectCarouselModel, Slider);

            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateDirectCarousel.cshtml", DirectCarouselModel);
        }
        [HttpPost]
        public ActionResult CreateDirectCarousel(DirectCarouselModel directCarouselModel)
        {
            if (ModelState.IsValid)
            {
                var slider = DirectCarouselModelToSliderEntity(directCarouselModel);
                _sliderService.InsertDirectCarousel(slider);
                SuccessNotification("Slider Created Successfullly!");
                return RedirectToRoute(new
                {
                    Action = "CreateDirectCarousel",
                    Controller = "Slider",
                    SliderId = slider.Id
                });
            }
            else
            {
                return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateDirectCarousel.cshtml");
            }
        }
        public ActionResult UpdateDirectCarousel(int SliderId)
        {
            Slider Slider = new Slider();
            Slider = _sliderService.GetSliderById(SliderId);
            DirectCarouselModel DirectCarouselModel = new DirectCarouselModel();
            PrepareDirectCarouselModel(DirectCarouselModel, Slider);

            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateDirectCarousel.cshtml", DirectCarouselModel);
        }
        [HttpPost]
        public ActionResult UpdateDirectCarousel(DirectCarouselModel directCarouselModel)
        {
            var sliderEntity = DirectCarouselModelToSliderEntity(directCarouselModel);
            Slider slider = _sliderService.GetSliderById(sliderEntity.Id);
            _sliderService.UpdateDirectCarousel(sliderEntity);
            SuccessNotification("Changed Save!");
            return View("~/Plugins/Widgets.NopSlider/Views/CreateUpdateDirectCarousel.cshtml", directCarouselModel);
        }

        #endregion

        #endregion

        #region Item
        [HttpPost]
        public ActionResult GetItemsList(int SliderId)
        {
            var sliderItems = _itemService.GetSliderItemsOrderbyDisplayOrder(SliderId);

            var gridModel = new DataSourceResult
            {
                Data = sliderItems.Select(x => new
                {
                    ItemId = x.Id,
                    Caption = x.Caption,
                    Url = x.Url,
                    FilePath = x.FilePath,
                    DisplayOrder = x.DisplayOrder
                }),
                Total = sliderItems.Count()
            };
            return Json(gridModel);
        }
        [ChildActionOnly]
        public ActionResult ManageItems(int SliderId = 0)
        {
            Item Item = new Item();
            Slider Slider = _sliderService.GetSliderById(SliderId);

            Item.Slider = Slider;
            Item.SliderId = SliderId;

            ItemModel ItemModel = new ItemModel();
            PrepareItemModel(ItemModel, Item);
            return View(ItemModel);
        }
        [HttpPost]
        public ActionResult CreateItem(ItemModel ItemModel)
        {
            var item = ItemModelToItemEntity(ItemModel);
            _itemService.InsertItem(item);
            SuccessNotification("Item Added!", false);
            return new NullJsonResult();
        }
        [HttpPost]
        public ActionResult UpdateItem(ItemModel itemModelUpdate)
        {
            var itemUpdate = ItemModelToItemEntity(itemModelUpdate);
            _itemService.UpdateItem(itemUpdate);
            SuccessNotification("Changed Save!");
            return new NullJsonResult();
        }
        [HttpPost]
        public ActionResult DeleteItem(int ItemId)
        {
            _itemService.DeleteItem(ItemId);

            return new NullJsonResult();
        }
        [ChildActionOnly]
        public ActionResult ManageItemsCarousel(int SliderId = 0)
        {
            Item Item = new Item();
            Slider slider = _sliderService.GetSliderById(SliderId);
            ViewBag.id = SliderId;

            Item.Slider = slider;
            Item.SliderId = SliderId;
            ItemModel ItemModel = new ItemModel();
            PrepareItemModel(ItemModel, Item);
            return View(ItemModel);
        }
        public ActionResult CreateItemCarousel(ItemModel ItemModel)
        {
            var item = ItemModelToItemEntity(ItemModel);
            _itemService.InsertNopItemCarousel(item);
            SuccessNotification("Item Added!", false);
            return new NullJsonResult();
        }

        #endregion

        #region Product
        public ActionResult List(int? SliderId)
        {
            if (SliderId == 0)
            {
                ViewBag.error = "Please save your widget before proceeding";
            }
            ViewBag.SliderId = SliderId;

            var model = new ProductListModel();
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //stores
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var wh in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = wh.Name, Value = wh.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }
        [HttpPost]
        public ActionResult ProductList(DataSourceRequest command, ProductListModel model)
        {
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.SearchVendorId = _workContext.CurrentVendor.Id;

            }
            var categoryIds = new List<int> { model.SearchCategoryId };
            //include subcategories
            if (model.SearchIncludeSubCategories && model.SearchCategoryId > 0)
                categoryIds.AddRange(GetChildCategoryIds(model.SearchCategoryId));
            var products = _productService.SearchProducts(
                categoryIds: categoryIds,
                manufacturerId: model.SearchManufacturerId,
                storeId: model.SearchStoreId,
                vendorId: model.SearchVendorId,

                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true
                );
            var gridModel = new DataSourceResult
            {
                Data = products.Select(x => new
                {
                    Id = x.Id,
                    Name = x.Name,
                    Published = x.Published
                }),
                Total = products.TotalCount
            };
            return Json(gridModel);

        }
        //[HttpPost]
        public ActionResult AddProductPopup(string btnId, string formId)
        {

            var allProducts = _productService.SearchProducts();
            if (allProducts == null)
                throw new ArgumentException("No product attribute mapping found with the specified id");
            var model = new NopSliderProductModel();


            //pictures
            foreach (var product in allProducts)
            {
                model = new NopSliderProductModel()
                {
                    PictureUrl = _pictureService.GetPictureUrl(product.ProductPictures.FirstOrDefault().PictureId)
                };
            }
            return View(model);
        }
        [HttpPost]
        public ActionResult ProductSave(ICollection<int> selectedIds, int SliderId)
        {
            Slider slider = _sliderService.GetSliderById(SliderId);

            bool isProduct = true;
            var products = new List<Product>();
            if (selectedIds != null)
            {
                products.AddRange(_productService.GetProductsByIds(selectedIds.ToArray()));
            }
            foreach (var item in products)
            {
                var nopItem = new Item();

                nopItem.FilePath = _pictureService.GetPictureUrl(item.ProductPictures.FirstOrDefault().PictureId);
                nopItem.ProductId = item.Id;
                slider.Items.Add(nopItem);
                _sliderRepo.Update(slider);
            }

            return Content("");
        }
        #endregion

        #region Configuration
        public ActionResult Configuration()
        {
            return View();
        }
        #endregion
        #endregion

        #region Store
        [AllowAnonymous]
        public ActionResult SliderWidget(string widgetZone)
        {
            var Slider = _sliderService.SliderBannerWidget(widgetZone);
            List<BannerModel> BannerModel = new List<BannerModel>();
            foreach (var slider in Slider)
            {
                BannerModel bannerModel = new BannerModel();
                PrepareBannerModel(bannerModel, slider);
                BannerModel.Add(bannerModel);
            }

            List<BannerModel> SingleImageBannerModel = new List<BannerModel>();
            List<BannerModel> MultiImageBannerModel = new List<BannerModel>();

            foreach (var item in BannerModel)
            {
                if (item.Items.Count == 1)
                {
                    SingleImageBannerModel.Add(item);
                }

                else
                {
                    MultiImageBannerModel.Add(item);
                }
            }

            if (SingleImageBannerModel.Count > 0)
                return View("~/Plugins/Widgets.NopSlider/Views/SliderWidgetSingleBanner.cshtml", SingleImageBannerModel);
            else
                return View(MultiImageBannerModel);

        }
        [AllowAnonymous]
        public ActionResult CategoryBannerWidget(string widgetZone, object additionalData = null)
        {
            int CategoryID = (int)additionalData;
            var Slider = _sliderService.SliderCategoryBannerWidget(widgetZone, CategoryID);

            if (Slider == null || Slider.Count == 0)
                return Content("");

            List<CategoryBannerModel> CategoryBannerModel = new List<CategoryBannerModel>();
            foreach (var slider in Slider)
            {
                CategoryBannerModel categoryBannerModel = new CategoryBannerModel();
                PrepareCategoryBannerModel(categoryBannerModel, slider);
                CategoryBannerModel.Add(categoryBannerModel);
            }

            List<CategoryBannerModel> SingleImageCategoryBanner = new List<CategoryBannerModel>();
            List<CategoryBannerModel> MultiImageCategoryBanner = new List<CategoryBannerModel>();
            
            foreach (var item in CategoryBannerModel)
            {
                if (item.Items.Count == 1)
                {
                    SingleImageCategoryBanner.Add(item);
                }

                else
                {
                    MultiImageCategoryBanner.Add(item);
                }
            }

            if (SingleImageCategoryBanner.Count > 0)
                return View("~/Plugins/Widgets.NopSlider/Views/CategoryBannerWidgetSingleImage.cshtml",
                    SingleImageCategoryBanner);
            else
                return View(MultiImageCategoryBanner);

        }
        [AllowAnonymous]
        public ActionResult CarouselWidget(int? productThumbPictureSize, int? SliderId)
        {
            ViewBag.SliderId = SliderId;
            Slider record = _sliderService.GetSliderById(SliderId);

            ViewBag.SliderName = record.SliderName;
            if (record.IsNavigationButton == true)
            {
                ViewBag.IsNavigationButton = 1;
                if (record.NavigationButtonPosition == "banner_top")
                {
                    ViewBag.NavigationButtonPosition = 1;
                }
                else
                {
                    ViewBag.NavigationButtonPosition = 0;
                }
            }
            else
            {
                ViewBag.IsNavigationButton = 0;
            }
            ViewBag.NoOfItems = record.NoOfItems;
            int NoOfItems = record.NoOfItems;


            if (record.IconChevronActive == true)
            {
                ViewBag.IconChevronActive = 1;
            }
            else
            {
                ViewBag.IconChevronActive = 0;
            }

            var items = _itemService.GetSliderItems(SliderId);
            var products = new List<Product>();
            foreach (var i in items)
            {
                if (i.ProductId != 0)
                {
                    var product = _productService.GetProductById(i.ProductId);
                    products.Add(product);
                }
            }
            //ACL and store mapping
            products = products.Where(p => _aclService.Authorize(p) && _storeMappingService.Authorize(p)).ToList();
            //availability dates
            products = products.Where(p => p.IsAvailable()).ToList();
            var model = (object)null;
            if (products.Count == 0)
                return Content("");

            if (record.IsLazyLoad == true)
            {
                model = PrepareProductOverviewModels(products, 1, NoOfItems, true, true, productThumbPictureSize).ToList();

            }
            else
            {
                model = PrepareProductOverviewModels(products, 0, NoOfItems, true, true, productThumbPictureSize).ToList();

            }


            return PartialView(model);
        }
        [AllowAnonymous]
        public ActionResult DirectCarouselWidget()
        {
            var models = _sliderService.DirectSliderWidget(); 
            return View(models);
        }
        #endregion

        #endregion
    }
}