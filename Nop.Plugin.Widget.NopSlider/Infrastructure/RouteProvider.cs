﻿using Nop.Web.Framework.Mvc.Routes;
using Nop.Web.Framework.Localization;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.NopSlider.Infrastructure
{
    class RouteProvider : IRouteProvider
    {
        public int Priority
        {
            get { return 1; }
        }

        public void RegisterRoutes(System.Web.Routing.RouteCollection routes)
        {
            ViewEngines.Engines.Insert(0, new SliderViewEngine());
            //Slider
            routes.MapLocalizedRoute("AddProductPopup", "Slider/List/{id}",
                            new { controller = "Slider", action = "List", SliderId = UrlParameter.Optional },
                            new[] { "Nop.Plugin.Widgets.NopSlider.Controllers" });
            //Slider Kendo Grid
            routes.MapLocalizedRoute("ProductList", "Slider/ProductList",
                           new { controller = "Slider", action = "ProductList" },
                           new[] { "Nop.Plugin.Widgets.NopSlider.Controllers" });
            routes.MapLocalizedRoute("CarouselWidget", "Slider/CarouselWidget",
                          new { controller = "Slider", action = "CarouselWidget", SliderId = UrlParameter.Optional, productThumbPictureSize = UrlParameter.Optional },
                          new[] { "Nop.Plugin.Widgets.NopSlider.Controllers" });
        }
            
    }
}
