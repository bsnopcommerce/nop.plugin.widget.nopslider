﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.NopSlider.Domain.Enum
{
    public enum ItemType
    {
        Banner = 0,
        CategoryBanner = 1,
        Carousel = 2
    }
}
