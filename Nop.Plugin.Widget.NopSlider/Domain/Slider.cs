﻿using FluentValidation.Attributes;
using Nop.Core;
using Nop.Plugin.Widgets.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Web.Framework;
using System.Collections.Generic;

namespace Nop.Plugin.Widget.NopSlider.Domain
{
    //[Validator(typeof(SliderValidator))]
    public class Slider : BaseEntity
    {
        #region Fields
        private ICollection<Item> _items;
        #endregion

        #region Constructor
        public Slider()
        {
            Items = new List<Item>();
        }
        #endregion

        #region Properties
        //public virtual int SliderId { get; set; }
        public virtual string SliderName { get; set; }
        public bool IsActive { get; set; }
        public virtual string ZoneName { get; set; }
        public virtual int Interval { get; set; }
        public int NoOfItems { get; set; }
        public virtual int? CategoryId { get; set; }
        public bool IsNavigationButton { get; set; }
        public string NavigationButtonPosition { get; set; }
        public SliderType SliderType { get; set; }
        public string SliderTypeName { get; set; }
        public bool IsProgressBar { get; set; }
        public string ProgressBarPostition { get; set; }
        public string TransitionStyle { get; set; }
        public bool IconChevronActive { get; set; }
        public bool IsMouseDragOn { get; set; }
        public bool isProduct { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsLazyLoad { get; set; }

        public virtual ICollection<Item> Items
        {
            get { return _items ?? (_items = new List<Item>()); }
            protected set { _items = value; }
        } 
        #endregion
    }
}