﻿using Nop.Web.Framework.Themes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Nop.Plugin.Widgets.NopSlider.Infrastructure
{
    class SliderViewEngine : ThemeableRazorViewEngine
    {
        public SliderViewEngine()
        {
            ViewLocationFormats = new[] { "~/Plugins/Widgets.NopSlider/Views/{0}.cshtml" };
            PartialViewLocationFormats = new[] { "~/Plugins/Widgets.NopSlider/Views/{0}.cshtml" };
        }

    }
}
