﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.NopSlider.Domain.Enum
{
    public enum SliderType
    {
        Banner = 0,
        CategoryBanner = 1,
        Carousel = 2,
        DirectCarousel = 3
    }
}
