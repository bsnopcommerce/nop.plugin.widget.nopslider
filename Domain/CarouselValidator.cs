﻿using FluentValidation;
using Nop.Plugin.Widgets.NopSlider.Model;
using Nop.Services.Localization;

namespace Nop.Plugin.Widgets.NopSlider.Domain
{
    class CarouselValidator : AbstractValidator<CarouselModel>
    {
        public CarouselValidator(ILocalizationService localizationService)
        {
            RuleFor(x => x.SliderName)
                .NotEmpty().WithMessage(localizationService.GetResource("plugins.misc.Nopslider.SliderNameRequired"));

            RuleFor(x => x.Interval)
                .NotEmpty().WithMessage(localizationService.GetResource("plugins.misc.Nopslider.SliderIntervalRequired"))
                .InclusiveBetween(1, 10).WithMessage(localizationService.GetResource("plugins.misc.Nopslider.GreaterThanZero"));
        }
    }
}
