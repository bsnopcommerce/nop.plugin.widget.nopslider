﻿using Nop.Core;
using System.ComponentModel.DataAnnotations;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;

namespace Nop.Plugin.Widget.NopSlider.Domain
{
    public class Item : BaseEntity
    {
        //[UIHint("Picture")]
        public virtual int PictureId { get; set; }
        public  int EntityId { get; set; }
        public int ProductId { get; set; }
        public string EntityName { get; set; }
        public virtual int SliderId { get; set; }
        public virtual string Caption { get; set; }
        public virtual string Url { get; set; }
        public virtual string FilePath { get; set; }
        public virtual int DisplayOrder { get; set; }
        public string ProductName { get; set; }
        public bool IsActive { get; set; }
        public ItemType ItemType { get; set; }
        public Slider Slider { get; set; }
    }
}