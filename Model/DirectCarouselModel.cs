﻿using FluentValidation.Attributes;
using Nop.Plugin.Widgets.NopSlider.Domain;
using Nop.Web.Framework;

namespace Nop.Plugin.Widgets.NopSlider.Model
{
    [Validator(typeof(DirectCarouselValidator))]
    public class DirectCarouselModel
    {
        public virtual int Id { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.SliderName")]
        public virtual string SliderName { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.IsActive")]
        public bool IsActive { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.interval")]
        public virtual int Interval { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.NoOfItems")]
        public int NoOfItems { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsNavigationButton")]
        public bool IsNavigationButton { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.NavigationButtonPosition")]
        public string NavigationButtonPosition { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IconChevronActive")]
        public bool IconChevronActive { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsLazyLoad")]
        public bool IsLazyLoad { get; set; }
    }
}
