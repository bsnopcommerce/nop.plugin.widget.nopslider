﻿using FluentValidation.Attributes;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Web.Framework;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.NopSlider.Model
{
    [Validator(typeof(CarouselModel))]
    public class CarouselModel
    {
        #region Constructor
        public CarouselModel()
        {
            Items = new List<ItemModel>();
        }
        #endregion

        #region Properties
        public virtual int Id { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.SliderName")]
        public virtual string SliderName { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.IsActive")]
        public bool IsActive { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.ZoneName")]
        public virtual string ZoneName { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.Interval")]
        public virtual int Interval { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.NoOfItems")]
        public int NoOfItems { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsNavigationButton")]
        public bool IsNavigationButton { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.NavigationButtonPosition")]
        public string NavigationButtonPosition { get; set; }
        public SliderType SliderType { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IconChevronActive")]
        public bool IconChevronActive { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsMouseDragOn")]
        public bool IsMouseDragOn { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.isProduct")]
        public bool IsProduct { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.DisplayOrder")]
        public int DisplayOrder { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsLazyLoad")]
        public bool IsLazyLoad { get; set; }
        public virtual IList<ItemModel> Items { get; set; }
        #endregion
    }
}
