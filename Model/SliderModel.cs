﻿using FluentValidation.Attributes;
using Nop.Plugin.Widgets.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.NopSlider.Model
{
    //[Validator(typeof(BannerValidator))]
    public class SliderModel
    {
        //#region Constructor
        //public SliderModel()
        //{
        //    Items = new List<SelectListItem>();
        //}
        //#endregion

        #region Properties
        public virtual int Id { get; set; }
        public virtual string SliderName { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.IsActive")]
        public bool IsActive { get; set; }
        //[NopResourceDisplayName("plugins.misc.Nopslider.ZoneName")]
        //public virtual string ZoneName { get; set; }
        //[NopResourceDisplayName("plugins.misc.Nopslider.Interval")]
        //public virtual int Interval { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.NoOfItems")]
        //public int NoOfItems { get; set; }
        //public virtual int? CategoryId { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.IsNavigationButton")]
        //public bool IsNavigationButton { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.NavigationButtonPosition")]
        //public string NavigationButtonPosition { get; set; }
        public SliderType SliderType { get; set; }
        public string SliderTypeName { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.IsProgressBar")]
        //public bool IsProgressBar { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.ProgressBarPostition")]
        //public string ProgressBarPostition { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.TransitionStyle")]
        //public string TransitionStyle { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IconChevronActive")]
        public bool IconChevronActive { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.IsMouseDragOn")]
        //public bool IsMouseDragOn { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.isProduct")]
        //public bool isProduct { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.DisplayOrder")]
        public int DisplayOrder { get; set; }
        //[NopResourceDisplayName("Plugins.Misc.NopSlider.IsLazyLoad")]
        //public bool IsLazyLoad { get; set; }

        //public virtual IList<SelectListItem> Items { get; set; }
        #endregion
    }
}
