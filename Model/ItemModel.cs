﻿using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.NopSlider.Model
{
    public class ItemModel
    {
        public virtual int Id { get; set; }
        [UIHint("Picture")]
        public virtual int PictureId { get; set; }
        public int EntityId { get; set; }
        public int ProductId { get; set; }
        //public string EntityName { get; set; }
        public virtual int SliderId { get; set; }
        public virtual string Caption { get; set; }
        public virtual string Url { get; set; }
        public virtual string FilePath { get; set; }
        public virtual int DisplayOrder { get; set; }
        //public string ProductName { get; set; }
        //public bool IsActive { get; set; }
        public ItemType ItemType { get; set; }
        public SliderModel Slider { get; set; }
    }
}
