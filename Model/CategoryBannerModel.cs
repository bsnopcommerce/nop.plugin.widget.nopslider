﻿using FluentValidation.Attributes;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Web.Framework;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Nop.Plugin.Widgets.NopSlider.Model
{
    [Validator(typeof(CategoryBannerModel))]
    public class CategoryBannerModel
    {
        public CategoryBannerModel()
        {
            CategoryList = new List<SelectListItem>();
            Items = new List<ItemModel>();
        }
        public virtual int Id { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.SliderName")]
        public virtual string SliderName { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.IsActive")]
        public bool IsActive { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.NoOfItems")]
        public int NoOfItems { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.Interval")]
        public virtual int Interval { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.CategoryId")]
        public virtual int? CategoryId { get; set; }
        [NopResourceDisplayName("plugins.misc.Nopslider.ZoneName")]
        public virtual string ZoneName { get; set; }
        public IList<SelectListItem> CategoryList { get; set; }
        public SliderType SliderType { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsNavigationButton")]
        public bool IsNavigationButton { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.NavigationButtonPosition")]
        public string NavigationButtonPosition { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsProgressBar")]
        public bool IsProgressBar { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.ProgressBarPostition")]
        public string ProgressBarPostition { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.TransitionStyle")]
        public string TransitionStyle { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IconChevronActive")]
        public bool IconChevronActive { get; set; }
        [NopResourceDisplayName("Plugins.Misc.NopSlider.IsMouseDragOn")]
        public bool IsMouseDragOn { get; set; }
        public virtual IList<ItemModel> Items { get; set; }
    }
}
