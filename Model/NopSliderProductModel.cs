﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using FluentValidation.Attributes;
using Nop.Web.Framework;

namespace Nop.Plugin.Widgets.NopSlider.Model
{
    public partial class NopSliderProductModel
    {
          
            [NopResourceDisplayName("Admin.Catalog.Products.Fields.ID")]
            public  int Id { get; set; }
            [NopResourceDisplayName("Admin.Catalog.Products.Fields.Name")]
            [AllowHtml]
            public string Name { get; set; }

            [NopResourceDisplayName("Admin.Catalog.Products.Fields.ShortDescription")]
            [AllowHtml]
            public string ShortDescription { get; set; }

            [NopResourceDisplayName("Admin.Catalog.Products.Fields.FullDescription")]
            [AllowHtml]
            public string FullDescription { get; set; }

            //pictures
            [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
            public string PictureUrl { get; set; }

//public partial class NopSliderProductPictureModel 
//    {
//        public int ProductId { get; set; }

//        [UIHint("Picture")]
//        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
//        public int PictureId { get; set; }

//        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.Picture")]
//        public string PictureUrl { get; set; }

//        [NopResourceDisplayName("Admin.Catalog.Products.Pictures.Fields.DisplayOrder")]
//        public int DisplayOrder { get; set; }
//    }
    }
    
        
}

