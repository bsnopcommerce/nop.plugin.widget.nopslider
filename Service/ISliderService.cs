﻿using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Plugin.Widgets.NopSlider.Model;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial interface ISliderService
    {
        #region Admin
        Slider GetSliderById(int? sliderId);
        List<Slider> GetAllSliders();

        #region Banner
        void InsertBanner(Slider banner);
        void UpdateBanner(Slider banner);
        #endregion

        #region CategoryBanner
        void InsertCategoryBanner(Slider categoryBanner);
        void UpdateCategoryBanner(Slider categoryBanner);
        #endregion

        #region Carousel
        void InsertCarousel(Slider carousel);
        void UpdateCarousel(Slider carousel);
        #endregion

        #region DirectCarousel
        void InsertDirectCarousel(Slider record);
        void UpdateDirectCarousel(Slider record);
        #endregion
        
        void UpdateSlider(Slider sliderUpdate);
        void DeleteSlider(int sliderId);
        #endregion

        #region Store
        List<Slider> SliderBannerWidget(string widgetZone);
        List<Slider> SliderCategoryBannerWidget(string widgetZone, int categoryId);
        List<Slider> DirectSliderWidget(); 
        #endregion
    }
}
