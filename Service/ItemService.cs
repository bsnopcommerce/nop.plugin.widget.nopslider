﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Services.Events;
using Nop.Core.Caching;
using Nop.Core.Data;
using Nop.Plugin.Widgets.NopSlider.Domain.Enum;
using Nop.Services.Media;

namespace Nop.Plugin.Widgets.NopSlider.Service
{
    public partial class ItemService : IItemService
    {
        #region Fields
        private ICacheManager _cacheService;
        private IEventPublisher _eventPublisher;
        private readonly IRepository<Item> _itemRepository;
        private readonly IPictureService _pictureService;
        private readonly IRepository<Slider> _sliderRepository;
        private readonly ISliderService _sliderService;

        private readonly ICacheManager _cacheManager;
        #endregion

        #region Constructor
        public ItemService(IRepository<Slider> sliderRepository,
            IRepository<Item> itemRepository, ISliderService sliderService,
            IEventPublisher eventPublisher, ICacheManager cacheManager, IPictureService pictureService, ICacheManager cacheManager1)
        {
            _itemRepository = itemRepository;
            this._cacheService = cacheManager;
            this._sliderRepository = sliderRepository;
            _sliderService = sliderService;
            _eventPublisher = eventPublisher;
            _pictureService = pictureService;
            _cacheManager = cacheManager1;
        }
        #endregion

        #region Methods
        public virtual ICollection<Item> GetSliderItems(int? sliderId)
        {
            var slider = _sliderService.GetSliderById(sliderId);
            return slider.Items;
        }
        public virtual IOrderedEnumerable<Item> GetSliderItemsOrderbyDisplayOrder(int sliderId)
        {
            return _sliderService.GetSliderById(sliderId).Items.OrderBy(x => x.DisplayOrder);
        }
        public virtual void InsertItem(Item item)
        {
            var slider = _sliderService.GetSliderById(item.SliderId);

            if (slider.SliderType == SliderType.Banner)
            {
                item.ItemType = ItemType.Banner;
            }
            else
                item.ItemType = ItemType.CategoryBanner;

            item.FilePath = _pictureService.GetPictureUrl(item.PictureId);

            slider.Items.Add(item);
            _sliderRepository.Update(slider);
        }
        public void InsertNopItemCarousel(Item item)
        {
            var slider = _sliderService.GetSliderById(item.SliderId);

            item.ItemType = ItemType.Carousel;
            slider.Items.Add(item);
            _sliderRepository.Update(slider);
        }
        public virtual void UpdateItem(Item itemUpdate)
        {
            Item item = _itemRepository.GetById(itemUpdate.PictureId);

            item.Caption = itemUpdate.Caption;
            item.Url = itemUpdate.Url;
            item.DisplayOrder = itemUpdate.DisplayOrder;
            _itemRepository.Update(item);
        }
        public void DeleteItem(int itemId)
        {
            Item item = _itemRepository.GetById(itemId);
            _itemRepository.Delete(item);
        } 
        #endregion

    }
}
