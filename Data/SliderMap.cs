﻿using Nop.Plugin.Widget.NopSlider.Domain;
using System.Data.Entity.ModelConfiguration;

namespace Nop.Plugin.Widget.NopSlider.Data
{
    public class SliderMap : EntityTypeConfiguration<Slider>
    {
        public SliderMap()
        {
            ToTable("NopSlider_NopSliders");

            HasKey(m => m.Id);

            Property(m => m.SliderName).IsRequired();
            Property(m => m.CategoryId).IsOptional();

            this.Ignore(m => m.SliderTypeName);          
        }
    }
}