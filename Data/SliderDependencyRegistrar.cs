﻿using Nop.Core.Infrastructure.DependencyManagement;
using Nop.Web.Framework.Mvc;
using Nop.Data;
using Autofac.Core;
using Nop.Plugin.Widget.NopSlider.Domain;
using Nop.Core.Data;
using Autofac;
using Nop.Core.Configuration;
using Nop.Core.Infrastructure;
using Nop.Plugin.Widgets.NopSlider.Service;

namespace Nop.Plugin.Widgets.NopSlider.Data
{
    public class SliderDependencyRegistrar : IDependencyRegistrar
    {
        #region Fields
        private const string CONTEXT_NAME = "nop_object_context_Nop_slider";
        #endregion

        #region Methods
        public int Order
        {
            get { return 1; }
        }

        public void Register(ContainerBuilder builder, ITypeFinder typeFinder, NopConfig config)
        {
            this.RegisterPluginDataContext<SliderObjectContext>(builder, CONTEXT_NAME);

            builder.RegisterType<EfRepository<Slider>>()
                .As<IRepository<Slider>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();

            builder.RegisterType<EfRepository<Item>>()
                .As<IRepository<Item>>()
                .WithParameter(ResolvedParameter.ForNamed<IDbContext>(CONTEXT_NAME))
                .InstancePerLifetimeScope();

            builder.RegisterType<SliderService>()
                .As<ISliderService>().
                InstancePerLifetimeScope();

            builder.RegisterType<ItemService>()
                .As<IItemService>().
                InstancePerLifetimeScope();
        } 
        #endregion
    }
}